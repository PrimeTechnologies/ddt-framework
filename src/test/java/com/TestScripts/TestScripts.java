package com.TestScripts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.BaseClass.BaseClass;

public class TestScripts extends BaseClass
{
	
	@Test(enabled=true)
	public void Tc1()
	{
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = "";

        // launch Fire fox and direct it to the Base URL
        driver.get(baseUrl);

        // get the actual value of the title
        actualTitle = driver.getTitle();

        /*
         * compare the actual title of the page with the expected one and print
         * the result as "Passed" or "Failed"
         */
        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
       
        //close Fire fox
        driver.close();
       
	}
	
	@Test(enabled=false)
	public void TestCase2() throws InterruptedException
	{
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		// find element by locator-name
		driver.findElement(By.name("firstname")).sendKeys("Bhanu");
		Thread.sleep(2000);
		// find element by locator-Id
		driver.findElement(By.id("u_0_7")).click();
		// find element by locator-link text
		driver.findElement(By.linkText("Sign Up")).click();
		
		driver.close();
		
		
	}

	@Test(enabled=false)
	public void TC3_textbox_radioButton()
	{
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		// text box
		driver.findElement(By.name("firstname")).sendKeys("Bhanu");
		
		//Radio button
		driver.findElement(By.id("u_0_7")).click();
		
		// check box
		
		driver.close();
	}
	
	@Test(enabled=false)
	public void TC4_dropdown()
	{
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		Select selectDropdown = new Select(driver.findElement(By.id("year")));
        List<WebElement> listOptionDropdown = selectDropdown.getOptions();
        
        // Count the item drop down list and assign into integer variable
        int dropdownCount = listOptionDropdown.size();//12
        
        // String month_val = listOptionDropdown.get(3).getText();//Mar
        for(int i = 0; i < dropdownCount ; i++)// 12<12 i=12;12<12=>false;12
        {
        	String optionsValue = listOptionDropdown.get(i).getText();//listOptionDropdown.get(11).getText()->Dec
            System.out.println(optionsValue);//Dec//
        }
        driver.close();   
	}
	
	@Test(enabled=false)
	public void TC5_AllLinks()
	{
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		// Only one link that too 1st link
		WebElement ele = driver.findElement(By.tagName("a"));
		System.out.println(ele);
		
		// fetch all the avaiable links from facebook.com page
		List<WebElement> links = driver.findElements(By.tagName("a"));
		 
		 System.out.println(links.size());
		 
		 for (int i = 0; i<links.size(); i++)
		 
		 {
		 
		 System.out.println(links.get(i).getText());
		 
		 
		 }
		 driver.close();
	}
	
	@Test(enabled=false)
	public void TC6_list()
	{
		//List->Interface,<String>,ArrayList->class
		// Webdriver driver = new chromedriver()
		List<String> vals = new ArrayList<String>();
		vals.add("Bhanu1");
		vals.add("Bhanu2");
		vals.add("Bhanu3");
		vals.add("Bhanu4");
		vals.add("Bhanu5");
		
		for (int i = 0; i < vals.size(); i++) 
		{
			System.out.println(vals.get(i).toString());
		}
	}
	
	
	@Test
	public void TC7_multipleWindow()
	{
		 //Launching the site.	
		 
         driver.get("http://demo.guru99.com/popup.php");			
         driver.manage().window().maximize();	
         // implicit wait
         
         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);       		
         driver.findElement(By.xpath("//*[contains(@href,'popup.php')]")).click();	
         
         WebDriverWait wait = new WebDriverWait(driver, 20);
         wait.until(ExpectedConditions.numberOfwindowsToBe(2));
        		 
        		
         String MainWindow=driver.getWindowHandle();//Parent window-12345ABCDE
        	System.out.println("Main Window ID is: " +MainWindow);	//12345ABCDE
        
        	// To handle all new opened window.				
        Set<String> s1 = driver.getWindowHandles();	//12345ABCDE, 2345XYZ	
        Iterator<String> i1=s1.iterator();		
        		
        while(i1.hasNext())		//hasnext()--boolean-true/false	
        {		
            String ChildWindow=i1.next();	
            System.out.println("Child Window ID is: " +ChildWindow);//12345ABCDE	--next()-String
            		
            if(!MainWindow.equalsIgnoreCase(ChildWindow))			
            {    		
                 
                    // Switching to Child window
                    driver.switchTo().window(ChildWindow);	                                                                                                           
                    driver.findElement(By.name("emailid"))
                    .sendKeys("gaurav.3n@gmail.com");                			
                    
                    driver.findElement(By.name("btnLogin")).click();			
                                 
                    // Closing the Child Window.
                    driver.close();		
            }		
        }		
        // Switching to Parent window i.e Main Window.
            driver.switchTo().window(MainWindow);
            driver.close();
            driver.quit();
    }
	
}

